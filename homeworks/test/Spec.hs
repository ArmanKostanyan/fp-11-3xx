-- Do not change! It will be overwritten!
-- Use MyTests.hs to define your tests

import Test.Tasty (TestTree(..), defaultMain, testGroup)

import AutoTests (exampleTests, autoTests)
import MyTests (myTests)

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests"
        [ testGroup "Example tests" exampleTests
        , testGroup "User defined tests" myTests
        , testGroup "Auto tests" autoTests
        ]

